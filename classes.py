import random
class Card:
    def __init__(self, number, suit):
        self.number=number
        self.suit=suit
    def __str__(self):
        return str(self.number) + " of "+self.suit
class Deck:
    def __init__(self):
        self.cards=[]
        for i in range(2,15):
            self.cards.append(Card(i,"spades"))
            self.cards.append(Card(i,"hearts"))
            self.cards.append(Card(i,"diamonds"))
            self.cards.append(Card(i,"clubs"))
    def shuffle(self):
        random.shuffle(self.cards)
    def draw(self):
        return self.cards.pop(0)
c=Card(10,"spades")
print(c.number)
print(c.suit)
print(c)
d=Card(2,"clubs")
print(d.number)
print(d.suit)
print(d)
dec=Deck()
dec.shuffle()
print(dec.draw())