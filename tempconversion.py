def F(Fc):
    return (Fc- 32)*1.8

def C(Cc):
    return Cc*1.8-32

print("This is a Temp converter that can convert F to C, and the other way around")
Rs=input("If you want to convert F to C press A. To convert the other way around press B: ")
if Rs=="A":
    Fc=int(input("Please put in a F to convert: "))
    print(F(Fc))

if Rs=="B":
    Cc=int(input("Please put in a C to convert: "))
    print(C(Cc))
